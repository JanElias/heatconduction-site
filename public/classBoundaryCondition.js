var classBoundaryCondition =
[
    [ "BoundaryCondition", "classBoundaryCondition.html#a5546b82ff47bafe887fcd62c3b3b60ac", null ],
    [ "~BoundaryCondition", "classBoundaryCondition.html#adf84546ce30fd54d10d34488c0148480", null ],
    [ "giveEdge", "classBoundaryCondition.html#a701854de6c2a8dd48a273fef59e6e5a7", null ],
    [ "giveKEntry", "classBoundaryCondition.html#a022cab9517a6a80a96ab4903a59336a7", null ],
    [ "giveTemperature", "classBoundaryCondition.html#a5a5f9d7fa6ed9c4c5facf8491dd9da26", null ],
    [ "giveTempGradient", "classBoundaryCondition.html#aeb8c21eaa8dd933183f5474b8f108d09", null ],
    [ "initialize", "classBoundaryCondition.html#a31c543612bf8a6ec80216ead9c7869a6", null ],
    [ "update", "classBoundaryCondition.html#a9faa0f03a6e01d41b1be251317e46779", null ],
    [ "currentTime", "classBoundaryCondition.html#a076a4f7a688c251efbdd110dee1ba956", null ],
    [ "edge", "classBoundaryCondition.html#a7ce00123422c5eb2552400aae1b54e61", null ],
    [ "func", "classBoundaryCondition.html#a67ae5a0506346f1fbb6e0be0de1add7c", null ]
];