var classRobinBC =
[
    [ "RobinBC", "classRobinBC.html#a15c2f21163a9157c38a573c59715cd08", null ],
    [ "~RobinBC", "classRobinBC.html#a3eb90734181d9654b75b3d3be92508ad", null ],
    [ "giveKEntry", "classRobinBC.html#a8324bff0fbd663f35e0e29a684b9f719", null ],
    [ "giveTemperature", "classRobinBC.html#a38cc6f0d4d27d2383641daaba787721f", null ],
    [ "giveTempGradient", "classRobinBC.html#aa05456ec6da5314256478d4e6a53b34c", null ],
    [ "initialize", "classRobinBC.html#a1ecf625c80637e19a81b5704a6a30bf6", null ],
    [ "readFromLine", "classRobinBC.html#a3b90550da022ddfe2857d006ac6b03ad", null ],
    [ "update", "classRobinBC.html#abf0db44942d14e9da777e9739ea3eaef", null ],
    [ "alpha", "classRobinBC.html#ae1329650ba456762d7b719044b76c9e7", null ],
    [ "alpha_func", "classRobinBC.html#a125961787f3b84d807893d7a20a55a4f", null ]
];