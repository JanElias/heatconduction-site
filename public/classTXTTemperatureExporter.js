var classTXTTemperatureExporter =
[
    [ "TXTTemperatureExporter", "classTXTTemperatureExporter.html#a6307b98e7f5c4e9bc80b2552bced0b4c", null ],
    [ "~TXTTemperatureExporter", "classTXTTemperatureExporter.html#a05d1c4a45c2a5e9fe6704b2551be6545", null ],
    [ "exportData", "classTXTTemperatureExporter.html#a6cd2c3c6c50c8cc436a447d455d34189", null ],
    [ "init", "classTXTTemperatureExporter.html#afe36e2f5158a05e7ce7536cdf960d323", null ],
    [ "readFromLine", "classTXTTemperatureExporter.html#a44ef8986e6c7080154dfa15e78c43583", null ],
    [ "cell", "classTXTTemperatureExporter.html#a5ab1b63bf1760cbd4052a76593ea68f2", null ],
    [ "dx", "classTXTTemperatureExporter.html#a353d3d7cfccacc7c7d17cef604d84e4c", null ],
    [ "dy", "classTXTTemperatureExporter.html#aaaa4d95ce5450782ef7390e36cbbb4c4", null ],
    [ "x", "classTXTTemperatureExporter.html#a3a41c15ceecec3facc538fdd65bdecbe", null ],
    [ "y", "classTXTTemperatureExporter.html#ab617739d19c065e3779bf8a5b0fe2db1", null ]
];