var classPipe =
[
    [ "Pipe", "classPipe.html#a91ddad07e89d5585b1cf27fa7860e201", null ],
    [ "~Pipe", "classPipe.html#af4519cc3f1f13857a724db7b2220eefa", null ],
    [ "addPipe", "classPipe.html#a018ea673880bc55cbcac98694e2ce14a", null ],
    [ "calcTempGradient", "classPipe.html#ab3f5d4a2369f151d82576667a5729697", null ],
    [ "giveUpwindAverageTemp", "classPipe.html#a025575f76b0b1f753543addb6841759c", null ],
    [ "update", "classPipe.html#a7ca3e65cf92d56ae34f9e279e3bbca28", null ],
    [ "isFirst", "classPipe.html#a7484dacddd450b3bb8ecfb04e0e6877e", null ],
    [ "pipes", "classPipe.html#a8dc9cc50d87a901fc73376c0521a4d3b", null ]
];