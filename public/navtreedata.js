/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "HeatConduction", "index.html", [
    [ "Getting started", "GettingStarted.html", [
      [ "How to \"install\" HeatConduction?", "GettingStarted.html#GettingStartedInstallation", [
        [ "Pre-requisites", "GettingStarted.html#Prereqisites", null ],
        [ "Build", "GettingStarted.html#Build", [
          [ "Cross Compilation", "GettingStarted.html#Crosscompilation", null ]
        ] ],
        [ "Build Documentation", "GettingStarted.html#Builddoc", null ]
      ] ],
      [ "Coding Standard", "GettingStarted.html#CodingStandard", null ],
      [ "Running HeatConduction", "GettingStarted.html#Running", [
        [ "OpenMP", "GettingStarted.html#OPENMP", null ],
        [ "Solvers", "GettingStarted.html#SOLVERS", null ]
      ] ]
    ] ],
    [ "Bibliography", "citelist.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"GettingStarted.html",
"classModelInstance.html#a9f3c7f99c1213725d82eac64336fc9b4",
"classTXTNodeContainerExporter.html#a6812258b1a886d185c4b6d15d0023b50"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';