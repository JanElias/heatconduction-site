var hierarchy =
[
    [ "BCContainer", "classBCContainer.html", null ],
    [ "BoundaryCondition", "classBoundaryCondition.html", [
      [ "NeumannBC", "classNeumannBC.html", null ],
      [ "RobinBC", "classRobinBC.html", [
        [ "DirichletBC", "classDirichletBC.html", null ],
        [ "NeumannRobinBC", "classNeumannRobinBC.html", [
          [ "FloorNeumannRobinBC", "classFloorNeumannRobinBC.html", null ]
        ] ]
      ] ]
    ] ],
    [ "Cell", "classCell.html", [
      [ "Pipe", "classPipe.html", null ]
    ] ],
    [ "CellContainer", "classCellContainer.html", null ],
    [ "DataExporter", "classDataExporter.html", [
      [ "ContainerExporter", "classContainerExporter.html", [
        [ "TXTCellContainerExporter", "classTXTCellContainerExporter.html", null ],
        [ "TXTEdgeContainerExporter", "classTXTEdgeContainerExporter.html", null ],
        [ "TXTNodeContainerExporter", "classTXTNodeContainerExporter.html", null ]
      ] ],
      [ "ObjectExporter", "classObjectExporter.html", [
        [ "TXTCellExporter", "classTXTCellExporter.html", null ],
        [ "TXTConvergenceCriteriaExporter", "classTXTConvergenceCriteriaExporter.html", null ],
        [ "TXTEdgeExporter", "classTXTEdgeExporter.html", null ],
        [ "TXTEdgeSumExporter", "classTXTEdgeSumExporter.html", [
          [ "TXTEdgeAvgExporter", "classTXTEdgeAvgExporter.html", null ],
          [ "TXTEdgeMaxExporter", "classTXTEdgeMaxExporter.html", null ],
          [ "TXTEdgeMinExporter", "classTXTEdgeMinExporter.html", null ],
          [ "TXTEdgeSumNormalizedByAreaExporter", "classTXTEdgeSumNormalizedByAreaExporter.html", null ]
        ] ],
        [ "TXTElapsedTimeExporter", "classTXTElapsedTimeExporter.html", null ],
        [ "TXTNodeExporter", "classTXTNodeExporter.html", null ],
        [ "TXTPipeConnectionExporter", "classTXTPipeConnectionExporter.html", null ],
        [ "TXTPipeConnectionSumExporter", "classTXTPipeConnectionSumExporter.html", null ],
        [ "TXTPipeTotalPerformanceExporter", "classTXTPipeTotalPerformanceExporter.html", null ],
        [ "TXTProblemSizeExporter", "classTXTProblemSizeExporter.html", null ],
        [ "TXTTemperatureExporter", "classTXTTemperatureExporter.html", null ]
      ] ],
      [ "TXTSolverExporter", "classTXTSolverExporter.html", null ],
      [ "VTKCellExporter", "classVTKCellExporter.html", null ],
      [ "VTKEdgeExporter", "classVTKEdgeExporter.html", null ]
    ] ],
    [ "Edge", "classEdge.html", null ],
    [ "EdgeContainer", "classEdgeContainer.html", null ],
    [ "ExporterContainer", "classExporterContainer.html", null ],
    [ "Function", "classFunction.html", [
      [ "ExponentialTimeStepFunction", "classExponentialTimeStepFunction.html", null ],
      [ "PieceWiseLinearFunction", "classPieceWiseLinearFunction.html", [
        [ "PeriodicPieceWiseLinearFunction", "classPeriodicPieceWiseLinearFunction.html", null ]
      ] ]
    ] ],
    [ "FunctionContainer", "classFunctionContainer.html", null ],
    [ "HeatSource", "classHeatSource.html", null ],
    [ "Model", "classModel.html", null ],
    [ "ModelInstance", "classModelInstance.html", null ],
    [ "Node", "classNode.html", null ],
    [ "NodeContainer", "classNodeContainer.html", null ],
    [ "PipeConnection", "classPipeConnection.html", null ],
    [ "Solver", "classSolver.html", [
      [ "SteadyStateNonLinearSolver", "classSteadyStateNonLinearSolver.html", [
        [ "SteadyStateLinearSolver", "classSteadyStateLinearSolver.html", [
          [ "TransientNonLinearSolver", "classTransientNonLinearSolver.html", [
            [ "TransientLinearSolver", "classTransientLinearSolver.html", null ]
          ] ]
        ] ]
      ] ]
    ] ]
];