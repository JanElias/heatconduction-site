var classNode =
[
    [ "Node", "classNode.html#a46e2960710d815db02469363e7830692", null ],
    [ "Node", "classNode.html#ad7a34779cad45d997bfd6d3d8043c75f", null ],
    [ "~Node", "classNode.html#aa0840c3cb5c7159be6d992adecd2097c", null ],
    [ "addToCoord", "classNode.html#af1120d22a0ce77e79449eb4f2e259295", null ],
    [ "giveCodeValue", "classNode.html#a63a8b2b423d83a5369e76eb60a8e05ae", null ],
    [ "giveCoord", "classNode.html#a2ad6eba57c5fe9d63091ee79159060e8", null ],
    [ "giveCoords", "classNode.html#a38a7a1e96e7c3630a6d2fdc10612a995", null ],
    [ "giveX", "classNode.html#a732d7949aa9cf0f67d8cd9826339780e", null ],
    [ "giveY", "classNode.html#aa328817d7b6e605b79050625f40107a1", null ],
    [ "giveZ", "classNode.html#a45603e9ce3bf88a5c3b0415754c3f024", null ],
    [ "norm", "classNode.html#aa8a2b9d450196499e73453e5f7066bdb", null ],
    [ "printCoords", "classNode.html#a03da7355fc5e862a1ac1e732f8265e3c", null ],
    [ "setCoord", "classNode.html#afd45449b74fc4865794810c4de9bd480", null ],
    [ "setX", "classNode.html#a08f58079ded691ebdea427bdc7fd30b8", null ],
    [ "setY", "classNode.html#a8c1387f8cec84df1ed47f2f56e643690", null ],
    [ "setZ", "classNode.html#a3e34b0104817584060e987797eb5d18c", null ]
];